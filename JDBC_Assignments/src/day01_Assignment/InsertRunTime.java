package day01_Assignment;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import common.db.DBConnection;

public class InsertRunTime {

	public static void main(String[] args) {
		
		Connection connection = DBConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmpID : ");
		int empId = scanner.nextInt();
		System.out.println("Enter EmpName : ");
		String empName = scanner.next();
		System.out.println("Enter Salary : ");
		double salary = scanner.nextDouble();
		System.out.println("Enter Gender : ");
		String gender = scanner.next();
		System.out.println("Enter EmailID : ");
		String emailId = scanner.next();
		System.out.println("Enter Password : ");
		String password = scanner.next();
		
		String insertQuery = "insert into employee value ( "
				+ empId+", '"+empName+"',"+ salary+", '"
				+gender+"', '"+emailId+"', '"+password+"')";
		
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			if(result > 0){
				System.out.println(result + " Record(s) Inserted...");
			}else{
				System.out.println("Records Insertion Failed......");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(connection != null){
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

}
