package day01_Assignment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import common.db.DBConnection;

public class FetchFromLocalStorage {

	public static void main(String[] args) {
		Connection connection = DBConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<Integer> empIDs = new ArrayList<>();
		
		String selectQuery = "select empId from employee";
		try {
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(selectQuery);
			if (resultSet != null){
				while(resultSet.next()){
//					System.out.print(resultSet.getInt(1) + " ");
					empIDs.add(resultSet.getInt(1));
				}
				Collections.sort(empIDs);
				System.out.println(empIDs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmpID :");
		int empId = scanner.nextInt();
		
		String selectQuery2 = "select * from employee where empId = " + empId;
		
		ResultSet resultSet2 = null;
		
		try {
			resultSet2 = statement.executeQuery(selectQuery2);
			if (resultSet2 != null){
				resultSet2.next();
				System.out.println("EmpID : "+ resultSet2.getInt(1));
				System.out.println("EmpName : "+ resultSet2.getString(2));
				System.out.println("Salary : "+ resultSet2.getDouble(3));
				System.out.println("Gender : "+ resultSet2.getString(4));
				System.out.println("EmailID : "+ resultSet2.getString(5));
				System.out.println("PAssword : "+ resultSet2.getString(6));
			}else{
				System.out.println("Enter VAlid EmpID.....");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			if(connection != null){
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
