package day01_Assignment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import common.db.DbBytetHost;

public class SelectByetHost {

	public static void main(String[] args) {
		Connection connection = DbBytetHost.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "select * from EmployeeTable";
		
		try {
			resultSet = connection.createStatement().executeQuery(selectQuery);
			if(resultSet != null){
				while(resultSet.next()){
					System.out.println("EmpID : "+ resultSet.getInt(1));
					System.out.println("EmpNAme : "+ resultSet.getString(2));
					System.out.println("Salary : "+ resultSet.getDouble(3));
					System.out.println("Gender : "+ resultSet.getString(4));
					System.out.println("EmailID : "+ resultSet.getString(5));
					System.out.println("PAssword : "+ resultSet.getString(6));
					System.out.println();
				}
			}else{
				System.out.println("Records Not Found to Fetch......");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(connection != null){
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
