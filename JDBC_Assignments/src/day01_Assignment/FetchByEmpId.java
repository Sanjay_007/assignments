package day01_Assignment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import common.db.DBConnection;

public class FetchByEmpId {

	public static void main(String[] args) {
		Connection connection = DBConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeID to Get Details : ");
		int empId = scanner.nextInt();
		
		String selectQuery = "select * from employee where empId = " + empId;
		
		try {
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(selectQuery);
			
			if(resultSet != null){
				resultSet.next();
				
				System.out.println("empId : "+resultSet.getInt(1));
				System.out.println("EmpName : "+ resultSet.getString(2));
				System.out.println("Salary : "+ resultSet.getDouble(3));
				System.out.println("Gender : "+ resultSet.getString(4));
				System.out.println("EmailID : "+ resultSet.getString(5));
				System.out.println("Password : "+ resultSet.getString(6));
			}else{
				System.out.println("Record not Found in the Table");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(connection != null){	
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
