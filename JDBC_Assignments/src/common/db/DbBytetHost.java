package common.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbBytetHost {
	public static Connection getConnection(){
		Connection connection = null;
		
		String url = "jdbc:mysql://cpanel.byethost7.com/b7_35930589_fsd_60";
		
		try {
			//1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//2. Establishing the Connection
			connection = DriverManager.getConnection(url, "root", "root");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
}
