import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{
  
  id:  number;
  name: string;
  age: number;
  address: any;
  hobbies: any;

  constructor(){
    alert("Constructor");

    this.id = 1001;
    this.name = 'Ram';
    this.age = 23;

    this.address = {
      streetNo:1001/1,
      city: 'Hyd',
      state: 'Telangana'
    }

    this.hobbies = ['Cricket', 'Chess', 'Reading', 'Football', 'VolleyBall'];
  }

  ngOnInit(){
      alert("ngOnInIt");
  }
}
