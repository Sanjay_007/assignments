package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {

	//Dependency Injection for StudentDao
	@Autowired
	StudentDao studentDao;

	@GetMapping("getAllStudents")
	public List<Student> getAllStudents(){
		List<Student> studentList = studentDao.getAllStudents();
		return studentList;
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int studId ){
		Student student = studentDao.getStudentById(studId);
		return student;
	}
	
	@GetMapping("getStudentByName/{sName}")
	public List<Student> getStudentByName(@PathVariable("sName") String studName){
		List<Student> studentList = studentDao.getStudentByName(studName);
		return studentList;
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student){
		Student stud = studentDao.addStudent(student);
		return stud;
	}
	
	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student student){
		Student stud = studentDao.updateStudent(student);
		return stud;
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int studId){
		studentDao.deleteStudentById(studId);
		return "Student with Id: "+studId+", Deleted Successfully";
	}
	
	
//	@GetMapping("studentLogin/{emailId}/{password}")
//	public String studentLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password){
//		return studentDao.studentLogin(emailId, password);
//	}
	
	@PostMapping("/student/login")
    public String studentLogin(@RequestBody Student loginRequest) {
        String studEmailId = loginRequest.getEmailId();
        String studPassword = loginRequest.getPassword();

        List<Student> authenticatedStudents = studentDao.studentLogin(studEmailId, studPassword);

        if (!authenticatedStudents.isEmpty()) {
            return "Student with emailId: " + studEmailId + ", logged in Successfully";
        } else {
            return "Login failed. Please check your email and password.";
        }
	}
	
}
