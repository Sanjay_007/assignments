package com.dao;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

	//Dependency Injection for StudentRepository
	@Autowired
	StudentRepository studentRepo;
	
	public List<Student> getAllStudents(){
		List<Student> studentList = studentRepo.findAll();
		return studentList;
	}

	public Student getStudentById(int studId) {
		Student student = studentRepo.findById(studId).orElse(null);
		return student;
	}

	public List<Student> getStudentByName(String studName) {
		List<Student> studentList = studentRepo.findByName(studName);
		return studentList;
	}

	public Student addStudent(Student student) {
		
		String encryptedPassword = new BCryptPasswordEncoder().encode(student.getPassword());
		student.setPassword(encryptedPassword);
		
		Student stud = studentRepo.save(student);
		return stud;
	}

	public Student updateStudent(Student student) {
		Student stud = studentRepo.save(student);
		return stud;
	}

	public void deleteStudentById(int studId) {
		studentRepo.deleteById(studId);
	}

//	public String studentLogin(String emailId, String password) {
//		return studentRepo.studentLogin(emailId, password);
//	}
	
	public List<Student> studentLogin(String studEmailId, String studPassword) {
		   
	    Student student = studentRepo.findByEmail(studEmailId);
	    
	    if (student != null) {
	        
	        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	        if (passwordEncoder.matches(studPassword, student.getPassword())) {
	            
	            return Collections.singletonList(student);
	        }
	    }
	    
	   
	    return Collections.emptyList();
	}
	
}
