package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Course {

	@Id
	private int cId;
	private String cName;
	private double fee;
	
	@JsonIgnore
	@OneToMany(mappedBy = "course")
	List<Student> studList = new ArrayList<Student>();
	
	public Course() {
	}

	public Course(int cId, String cName, double fee) {
		this.cId = cId;
		this.cName = cName;
		this.fee = fee;
	}

	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public List<Student> getStudList() {
		return studList;
	}

	public void setStudList(List<Student> studList) {
		this.studList = studList;
	}

	@Override
	public String toString() {
		return "Course [cId=" + cId + ", cName=" + cName + ", fee=" + fee + ", studList=" + studList + "]";
	}
	
}
