package com.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);
		
		if (emailId.equalsIgnoreCase("Admin") && password.equals("admin")){
			request.getRequestDispatcher("AdminHomePage.jsp").forward(request, response);
		}else{
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
