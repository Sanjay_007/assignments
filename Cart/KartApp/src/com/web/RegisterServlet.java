package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CustomerDao;
import com.dto.Customer;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		String custName = request.getParameter("custName");
		String mobileNumber = request.getParameter("mobileNumber");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		Customer customer = new Customer(custName, mobileNumber, gender, emailId, password);
		CustomerDao customerDao = new CustomerDao();
		int result = customerDao.registerCustomer(customer);
		
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		if(result > 0){
			out.print("<h1>Employee Registration Success</h1>");
			request.getRequestDispatcher("Login.html").forward(request, response);
		}else{
			out.print("<h1 style='color:red'>Employee Registration Failed...</h1>");
			request.getRequestDispatcher("Register.html").include(request, response);
		}
		out.print("<center></body>");

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
