package com.dto;

public class Customer {
	
	private String custName;
	private String mobileNumber;
	private String gender;
	private String emailId;
	private String password;
	public Customer() {
		super();
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobilNumber) {
		this.mobileNumber = mobilNumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Customer [custName=" + custName + ", mobileNumber=" + mobileNumber + ", gender=" + gender + ", emailId="
				+ emailId + ", password=" + password + "]";
	}
	public Customer(String custName, String mobileNumber, String gender, String emailId, String password) {
		super();
		this.custName = custName;
		this.mobileNumber = mobileNumber;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}
	
	
	
}
