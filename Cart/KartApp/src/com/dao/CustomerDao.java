package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.db.DbConnection;
import com.dto.Customer;

public class CustomerDao {

	public int registerCustomer(Customer customer) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String registerQuery = "insert into customer (custName, mobileNumber, gender, emailId, password) values(?,?,?,?,?)";
		
		try {
			pst = con.prepareStatement(registerQuery);
			
			pst.setString(1, customer.getCustName());
			pst.setString(2, customer.getMobileNumber());
			pst.setString(3, customer.getGender());
			pst.setString(4, customer.getEmailId());
			pst.setString(5, customer.getPassword());
			
			// int result = preparedStatement.executeUpdate();
			return pst.executeUpdate();
 			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

}
