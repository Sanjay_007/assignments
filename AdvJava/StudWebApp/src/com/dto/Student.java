package com.dto;

public class Student {
	private int studId;
	private String StudName;
	private String gender;
	private String emailId;
	private String password;
	
	public Student(){
		super();
	}

	public Student(int studId, String studName, String gender, String emailId, String password) {
		super();
		this.studId = studId;
		StudName = studName;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getStudId() {
		return studId;
	}

	public void setStudId(int studId) {
		this.studId = studId;
	}

	public String getStudName() {
		return StudName;
	}

	public void setStudName(String studName) {
		StudName = studName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
