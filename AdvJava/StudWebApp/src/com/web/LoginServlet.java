package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.StudentDao;
import com.dto.Student;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		//Store emailId under HttpSession object (true = New Session will be created)
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);
		
		PrintWriter out = response.getWriter();
		
		out.print("<html>");
		out.print("<body bgcolor = 'lightgray'>");
		out.print("<center>");
		if(emailId.equalsIgnoreCase("s") && password.equals("s")){
			out.print("<h1>Login Successful...</h1>");
			
			//Calling StudentHomePage..
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentHomePage");
			requestDispatcher.forward(request, response);
		}else{
			
			StudentDao studentDao = new StudentDao();
			Student student = studentDao.studLogin(emailId,password);
			if (student != null) {
				
				//Storing Employee Object Under Session
				session.setAttribute("student", student);
				
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudHomePage");
				requestDispatcher.forward(request, response);
			} else {
				out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
				requestDispatcher.include(request, response);
			}
		}
		out.print("</center>");
		out.print("</body>");
		out.print("</html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
