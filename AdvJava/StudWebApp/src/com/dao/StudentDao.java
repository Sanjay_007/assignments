package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.db.DbConnect;
import com.dto.Student;
import java.util.List;

public class StudentDao {

	public Student studLogin(String emailId, String password) {
		
		Connection connection = DbConnect.getConnect();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String loginQuery = "select * from student where emailId=? and password=?";
		
		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			preparedStatement.setString(1, emailId);
			preparedStatement.setString(2, password);
			
			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next()){
				Student student = new Student();
				
				student.setStudId(resultSet.getInt(1));
				student.setStudName(resultSet.getString(2));
				student.setGender(resultSet.getString(3));
				student.setEmailId(resultSet.getString(4));
				student.setPassword(resultSet.getString(5));
				
				
				return  student;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public List<Student> getAllStudents() {

		Connection connection = DbConnect.getConnect();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
			
		String loginQuery = "Select * from student";
		List<Student> studentList = new ArrayList<Student>();
			
		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {
				
				while (resultSet.next()) {
						
					Student student = new Student();
						
					student.setStudId(resultSet.getInt(1));
					student.setStudName(resultSet.getString(2));
					student.setGender(resultSet.getString(3));
					student.setEmailId(resultSet.getString(4));
					student.setPassword(resultSet.getString(6));
					
					studentList.add(student);
				}
				
				return studentList;
			} 
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return null;
	}

	
	
}
